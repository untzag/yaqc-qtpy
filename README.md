# yaqc-qtpy

[![PyPI](https://img.shields.io/pypi/v/yaqc-qtpy)](https://pypi.org/project/yaqc-qtpy)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqc-qtpy)](https://anaconda.org/conda-forge/yaqc-qtpy)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqc-qtpy/-/blob/master/CHANGELOG.md)

Tooling for building simple yaq clients using qtpy.

```bash
yaqd list --format json | yaqc-qtpy -
```